function countLetter(letter, sentence) {
    if (typeof letter === 'string' && letter.length === 1) {
        let result = 0;
        for (const char of sentence) {
            if (char === letter) {
                result++;
            }
        }
        return result;
    }
    return undefined    

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    const lowerCaseText = text.toLowerCase();
    const charSet = new Set();
    for (const char of lowerCaseText) {
        if (charSet.has(char)) {
            return false;
        }
        charSet.add(char);
    }
    return true;

    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
}

function purchase(age, price) {
    if (age < 13) {
        return undefined;
    }
    let discount = age >=13 && age <= 21 || age > 64 ? 0.8 : 1;
    let discountedPrice = Math.round(price*discount*100) / 100;
    return discountedPrice.toFixed(2);
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {

    const hotCategories = new Set();
    for (const item of items) {
        if (item.stocks === 0) {
            hotCategories.add(item.category);
        }
    }
    return Array.from(hotCategories);

    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    const candidateASet = new Set(candidateA);
    const flyingVoters = [];
    for (const voter of candidateB) {
        if (candidateASet.has(voter)) {
            flyingVoters.push(voter);
        }
    }
    return flyingVoters.sort();
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};